jQuery ($) ->
  $(document).ready ->

    # fastclick
    FastClick.attach document.body

    # site nav
    $("main").click ->
        $("body").removeClass("is-open")
        $("#nav-toggle").removeClass("is-open")
        $("#nav").removeClass("is-open")

    $("#nav-toggle").click (e) ->
        e.stopPropagation()
        $("body").toggleClass("is-open")
        $(this).toggleClass("is-open")
        $("#nav").toggleClass("is-open")